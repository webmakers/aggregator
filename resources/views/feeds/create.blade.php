@extends('layouts.feed')

@section('content_feed')
    <h1>Add a New Feed</h1>
    <p class="lead">Add to your feeds list below.</p>
    <hr>
    @include('errors.form_errors')
    {!! Form::open(['route' => 'feeds.store']) !!}
    <div class="form-group">
        {!! Form::label('title', 'Title:', ['class' => 'control-label']) !!}
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('title', 'Title:', ['class' => 'control-label']) !!}
        {!! Form::select('feed_category_id', $categories, \Illuminate\Support\Facades\Input::old('feed_category_id'), ['class'=> 'form-control'])  !!}
    </div>
    <div class="form-group">
        {!! Form::label('url', 'Url:', ['class' => 'control-label']) !!}
        {!! Form::text('url', null, ['class' => 'form-control']) !!}
    </div>
    {!! Form::submit('Create New Feed', ['class' => 'btn btn-primary']) !!}

    {!! Form::close() !!}
@stop
