<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class FeedNews
 * @package App
 */
class FeedNews extends Model
{
    /** @var string */
    protected $primaryKey = 'uid';
    /** @var array */
    protected $fillable = [
        'category',
        'link',
        'title',
        'description',
        'feed_id',
        'uid',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function feed()
    {
        return $this->belongsTo(Feed::class);
    }
}
