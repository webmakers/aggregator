<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * Class Feed
 *
 * @package App
 * @property integer $id
 * @property string $title
 * @property string $url
 * @property integer $feed_category_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Feed whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Feed whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Feed whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Feed whereFeedCategoryId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Feed whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Feed whereUpdatedAt($value)
 */
	class Feed extends \Eloquent {}
}

namespace App{
/**
 * Class FeedCategory
 *
 * @package App
 * @property integer $id
 * @property string $title
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Feed[] $feeds
 * @method static \Illuminate\Database\Query\Builder|\App\FeedCategory whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FeedCategory whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FeedCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FeedCategory whereUpdatedAt($value)
 */
	class FeedCategory extends \Eloquent {}
}

namespace App{
/**
 * Class FeedNews
 *
 * @package App
 * @property string $uid
 * @property string $category
 * @property string $link
 * @property string $title
 * @property string $description
 * @property integer $feed_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Feed $feeds
 * @method static \Illuminate\Database\Query\Builder|\App\FeedNews whereUid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FeedNews whereCategory($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FeedNews whereLink($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FeedNews whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FeedNews whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FeedNews whereFeedId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FeedNews whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FeedNews whereUpdatedAt($value)
 */
	class FeedNews extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $unreadNotifications
 * @method static \Illuminate\Database\Query\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

