<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Feed
 * @package App
 */
class Feed extends Model
{
    /** @var array */
    protected $fillable = [
        'title',
        'url',
        'feed_category_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function feedNews()
    {
        return $this->hasMany(FeedNews::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(FeedCategory::class);
    }
}
