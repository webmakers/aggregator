<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CreateTableFeedNews
 */
class CreateTableFeedNews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feed_news', function (Blueprint $table) {
            $table->string('uid')->unique()->length(32)->index('uid');
            $table->string('category');
            $table->string('link');
            $table->string('title');
            $table->longText('description');
            $table->unsignedInteger('feed_id');
            $table->foreign('feed_id')->references('id')->on('feeds');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('feed_news');
    }
}
