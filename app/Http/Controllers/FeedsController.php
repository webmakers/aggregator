<?php
namespace App\Http\Controllers;

use App\Feed;
use App\FeedCategory;
use App\Http\Requests;
use App\Http\Requests\FeedsRequest;
use Session;

/**
 * Class FeedsController
 * @package App\Http\Controllers
 */
class FeedsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $feeds = Feed::all();

        return view('feeds.index')->with('feeds', $feeds);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $categories = FeedCategory::pluck('title', 'id');
        return view('feeds.create')->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param FeedsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(FeedsRequest $request)
    {
        $input = $request->all();
        Feed::create($input);

        Session::flash('flash_message', 'Feed successfully added!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        /** @var Feed $feed */
        $feed = Feed::findOrFail($id);

        return view('feeds.show')->with('feed', $feed);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        /** @var Feed $feed */
        $feed = Feed::findOrFail($id);
        $categories = FeedCategory::pluck('title', 'id');
        return view('feeds.edit', [
            'feed' => $feed,
            'categories' => $categories
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param FeedsRequest $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(FeedsRequest $request, $id)
    {
        /** @var Feed $feed */
        $feed = Feed::findOrFail($id);
        $input = $request->all();
        $feed->fill($input)->save();

        Session::flash('flash_message', 'Feed successfully updated!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        /** @var Feed $feed */
        $feed = Feed::findOrFail($id);
        $feed->delete();

        Session::flash('flash_message', 'Feed successfully deleted!');
        return redirect()->route('feeds.index');
    }
}
