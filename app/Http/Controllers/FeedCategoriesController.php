<?php
namespace App\Http\Controllers;

use App\FeedCategory;
use App\Http\Requests;
use App\Http\Requests\FeedCategoriesRequest;
use Session;

/**
 * Class FeedCategoriesController
 * @package App\Http\Controllers
 */
class FeedCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $categories = FeedCategory::all();

        return view('feed_categories.index')->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('feed_categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param FeedCategoriesRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(FeedCategoriesRequest $request)
    {
        $input = $request->all();
        FeedCategory::create($input);

        Session::flash('flash_message', 'Category successfully added!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        /** @var FeedCategory $category */
        $category = FeedCategory::findOrFail($id);
        $feeds = $category->feeds()->getResults();

        return view('feed_categories.show',
            [
                'category' => $category,
                'feeds' => $feeds,
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        /** @var FeedCategory $category */
        $category = FeedCategory::findOrFail($id);

        return view('feed_categories.edit')->with('category', $category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param FeedCategoriesRequest $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(FeedCategoriesRequest $request, $id)
    {
        /** @var FeedCategory $category */
        $category = FeedCategory::findOrFail($id);
        $input = $request->all();
        $category->fill($input)->save();

        Session::flash('flash_message', 'Category successfully updated!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        /** @var FeedCategory $category */
        $category = FeedCategory::findOrFail($id);
        $category->feeds()->delete();
        $category->delete();

        Session::flash('flash_message', 'Category successfully deleted!');
        return redirect()->route('feed-categories.index');
    }
}
