@extends('layouts.feed')

@section('content_feed')
    <h1>Feed "{{ $feed->title }}"</h1>
    <hr>

    <div class="row">
        <div class="col-md-6">
            <a href="{{ route('feeds.index') }}" class="btn btn-info">Back to all feeds</a>
            <a href="{{ route('feeds.edit', $feed->id) }}" class="btn btn-primary">Edit feed</a>
        </div>
        <div class="col-md-6 text-right">
            {!! Form::open([
                'method' => 'DELETE',
                'route' => ['feeds.destroy', $feed->id]
            ]) !!}
            {!! Form::submit('Delete this feed?', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@stop
