@extends('layouts.feed')

@section('content_feed')

    <h1>Editing "{{ $feed->title }}"</h1>
    <p class="lead">Edit this feed below, or <a href="{{ route('feeds.index') }}">go back to all feeds.</a></p>
    <hr>

    @include('errors.form_errors')
    {!! Form::model($feed, [
        'method' => 'PATCH',
        'route' => ['feeds.update', $feed->id]
    ]) !!}
    <div class="form-group">
        {!! Form::label('title', 'Title:', ['class' => 'control-label']) !!}
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('title', 'Title:', ['class' => 'control-label']) !!}
        {!! Form::select('feed_category_id', $categories, \Illuminate\Support\Facades\Input::old('feed_category_id'), ['class'=> 'form-control'])  !!}
    </div>
    <div class="form-group">
        {!! Form::label('url', 'Url:', ['class' => 'control-label']) !!}
        {!! Form::text('url', null, ['class' => 'form-control']) !!}
    </div>
    {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
    {!! Form::close() !!}
@stop
