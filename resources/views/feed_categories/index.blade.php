@extends('layouts.feed-category')

@section('content_category')
    @foreach($categories as $category)
        <h3>{{ $category->title }}</h3>
        <p>
            <a href="{{ route('feed-categories.show', $category->id) }}" class="btn btn-info">View</a>
            <a href="{{ route('feed-categories.edit', $category->id) }}" class="btn btn-primary">Edit</a>
        </p>
        <hr>
    @endforeach
@stop
