@extends('layouts.feed-category')

@section('content_category')
    <h1>Category "{{ $category->title }}"</h1>
    <hr>

    <div class="row">
        <div class="col-md-6">
            <a href="{{ route('feed-categories.index') }}" class="btn btn-info">Back to all categories</a>
            <a href="{{ route('feed-categories.edit', $category->id) }}" class="btn btn-primary">Edit category</a>
        </div>
        <div class="col-md-6 text-right">
            {!! Form::open([
                'method' => 'DELETE',
                'route' => ['feed-categories.destroy', $category->id]
            ]) !!}
            {!! Form::submit('Delete this category?', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </div>
    </div>
    @if (count($feeds))
        <div class="row">
            <h2>In category "{{ $category->title }}" existing feeds</h2>
            <div class="col-md-6">
                @foreach($feeds as $feed)
                    <h3>{{ $feed->title }}, {{ $feed->url }}</h3>
                    <p>
                        <a href="{{ route('feed-categories.show', $feed->id) }}" class="btn btn-info">View</a>
                        <a href="{{ route('feed-categories.edit', $feed->id) }}" class="btn btn-primary">Edit</a>
                    </p>
                    <hr>
                @endforeach
            </div>
        </div>
    @endif
@stop
