@extends('layouts.app')

@section('title')
    {{ config('app.name', 'News aggregator') }}
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div id="category-container" class="col-md-1">
                @foreach($categories as $category)
                    <h4>
                        <a href="/category/{{ $category->id }}">
                            <span class="label @if($category->id == $selected_category)label-primary @else label-info @endif">
                                {{ $category->title }}
                            </span>
                        </a>
                    </h4>
                @endforeach
            </div>
            <div class="col-md-8 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="list-group">
                        @foreach($news as $new)
                            <div class="list-group-item">
                                <h4 class="list-group-item-heading list-group-item list-group-item-info"
                                    data-toggle="modal" data-target="#feed-modal"
                                    data-description="{{ $new->description }}"
                                    data-link="{{ $new->link }}"
                                    data-title="{{ $new->title }}">{{ $new->title }}</h4>
                            </div>
                        @endforeach
                    </div>
                </div>
                {{ $news->links() }}
            </div>
            <div id="feed-providers-container" class="col-md-1">
                @foreach($feeds as $feed)
                    <h4>
                        <a href="{{ $feed->url }}" target="_blank">
                            <span class="label label-info">
                                {{ $feed->title }}
                            </span>
                        </a>
                    </h4>
                @endforeach
            </div>
            <div id="feed-modal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default">
                                <a href="#" target="_blank">Go to feed page</a>
                            </button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
                    $('#feed-modal').on('show.bs.modal', function (event) {
                        var button = $(event.relatedTarget);
                        var title = button.data('title');
                        var description = button.data('description');
                        var link = button.data('link');
                        var modal = $(this);
                        modal.find('.modal-title').text(title);
                        modal.find('.modal-body').text(description);
                        modal.find('.modal-footer a').attr('href', link);
                    })
                }
        );
    </script>
@endsection
