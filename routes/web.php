<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/', 'HomeController@index');
Route::get('/category/{id}', 'HomeController@category');
Route::get('/change-password', 'Auth\ChangePasswordController@showChangePasswordForm');
Route::post('/change-password', 'Auth\ChangePasswordController@changePassword');

Route::resource('feeds', 'FeedsController');
Route::resource('feed-categories', 'FeedCategoriesController');
