@extends('layouts.feed-category')

@section('content_category')
    <h1>Add a new category</h1>
    <p class="lead">Add to your categories list below.</p>
    <hr>
    @include('errors.form_errors')
    {!! Form::open(['route' => 'feed-categories.store']) !!}

    <div class="form-group">
        {!! Form::label('title', 'Title:', ['class' => 'control-label']) !!}
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>

    {!! Form::submit('Create new category', ['class' => 'btn btn-primary']) !!}

    {!! Form::close() !!}
@stop
