<?php
namespace Tests\Unit\Services;

use TestCase;

/**
 * Class ValidationTest
 * @package Tests\Unit\Services
 */
class ValidationTest extends TestCase
{
    /**
     * @test
     */
    public function valid_current_password()
    {
        $pass = 'password';
        $hash = password_hash($pass, PASSWORD_BCRYPT);
        $rules = [
            'current-password' => 'current_password:user,current_password,' . $hash,
        ];

        $data = [
            'current-password' => $pass,
        ];

        $validator = $this->app->make('validator')->make($data, $rules);
        $this->assertTrue($validator->passes());
    }

    /**
     * @test
     */
    public function invalid_current_password()
    {
        $pass = 'password';
        $rules = [
            'current-password' => 'current_password:user,current_password,' . $pass,
        ];

        $data = [
            'current-password' => $pass,
        ];

        $validator = $this->app->make('validator')->make($data, $rules);
        $this->assertFalse($validator->passes());
    }
}
