<?php
namespace App\Http\Requests;

use App\Feed;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class FeedsRequest
 * @package App\Http\Requests
 */
class FeedsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $append = '';
        switch ($this->method()) {
            case 'PATCH': {
                $append = ',url,' . $this->feed;
                break;
            }
        }
        return [
            'title' => 'required|max:255',
            'feed_category_id' => 'required|integer|exists:feed_categories,id',
            'url' => 'required|max:255|url|active_url|unique:feeds' . $append,
        ];
    }
}
