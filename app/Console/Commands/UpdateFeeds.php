<?php
namespace App\Console\Commands;

use App\Feed;
use App\Services\FeedService;
use Illuminate\Console\Command;

/**
 * Class UpdateFeeds
 * @package App\Console\Commands
 */
class UpdateFeeds extends Command
{
    /** @var string */
    protected $signature = 'feed:update';
    /** @var string */
    protected $description = 'Update feed';
    /** @var FeedService */
    private $feedService;

    /**
     * UpdateFeeds constructor.
     * @param FeedService $feedService
     */
    public function __construct(FeedService $feedService)
    {
        parent::__construct();
        $this->feedService = $feedService;
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        /** @var Feed[] $feeds */
        $feeds = Feed::all();
        $bar = $this->output->createProgressBar(count($feeds));
        foreach ($feeds as $feed) {
            $feedsData = $this->feedService->getNews($feed);
            $this->feedService->saveNew($feedsData);
            $bar->advance();
        }
        $bar->finish();
    }
}
