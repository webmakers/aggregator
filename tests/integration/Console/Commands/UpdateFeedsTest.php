<?php
namespace Tests\Integration\Console\Commands;

use App\Console\Commands\UpdateFeeds;
use App\Feed;
use App\FeedCategory;
use App\FeedNews;
use DB;
use Symfony\Component\Console\Application as ConsoleApplication;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;
use TestCase;

/**
 * Class UpdateFeedsTest
 * @package Tests\Integration\Console\Commands
 */
class UpdateFeedsTest extends TestCase
{
    /** @var Command */
    private $command;
    /** @var CommandTester */
    private $commandTester;

    public function setUp()
    {
        parent::setUp();
        DB::beginTransaction();
        $application = new ConsoleApplication();
        $testedCommand = $this->app->make(UpdateFeeds::class);
        $testedCommand->setLaravel(app());
        $application->add($testedCommand);

        $this->command = $application->find('feed:update');
        $this->commandTester = new CommandTester($this->command);
    }

    public function tearDown()
    {
        DB::rollback();
    }

    /**
     * @test
     */
    public function fetch_feeds()
    {
        DB::table('feed_news')->delete();
        DB::table('feeds')->delete();
        DB::table('feed_categories')->delete();
        $this->assertEmpty(FeedNews::all());
        $this->assertEmpty(Feed::all());
        $this->assertEmpty(FeedCategory::all());
        $feedCategory = FeedCategory::create([
            'title' => 'sports',
        ]);
        $this->assertCount(1, FeedCategory::all());
        Feed::create([
            'title' => 'sportas',
            'url' => 'http://sportas.lrytas.lt/rss/',
            'feed_category_id' => $feedCategory->id,
        ]);
        $this->assertCount(1, FeedCategory::all());
        $this->commandTester->execute([
            'command' => $this->command->getName(),
        ]);
        $this->assertRegExp('/100%/', $this->commandTester->getDisplay());

        $this->assertNotEmpty(FeedNews::all());
    }
}
