@extends('layouts.feed')

@section('content_feed')
    @foreach($feeds as $feed)
        <h3>{{ $feed->title }}</h3>
        <p>
            <a href="{{ route('feeds.show', $feed->id) }}" class="btn btn-info">View</a>
            <a href="{{ route('feeds.edit', $feed->id) }}" class="btn btn-primary">Edit</a>
        </p>
        <hr>
    @endforeach
@stop
