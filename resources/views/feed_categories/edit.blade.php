@extends('layouts.feed-category')

@section('content_category')

    <h1>Editing "{{ $category->title }}"</h1>
    <p class="lead">Edit this category below, or <a href="{{ route('feed-categories.index') }}">go back to all categories.</a></p>
    <hr>

    @include('errors.form_errors')
    {!! Form::model($category, [
        'method' => 'PATCH',
        'route' => ['feed-categories.update', $category->id]
    ]) !!}

    <div class="form-group">
        {!! Form::label('title', 'Title:', ['class' => 'control-label']) !!}
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>

    {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}

    {!! Form::close() !!}
@stop
