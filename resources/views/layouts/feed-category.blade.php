@extends('layouts.app')

@section('title')
    Feed Categories
@endsection
@section('content')
    <div class="container">
        <div class="row">
            @if(Session::has('flash_message'))
                <div class="alert alert-success">
                    {{ Session::get('flash_message') }}
                </div>
            @endif

            @yield('content_category')
        </div>
    </div>
@endsection
