<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class FeedCategory
 * @package App
 */
class FeedCategory extends Model
{
    /** @var array */
    protected $fillable = [
        'title',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function feeds()
    {
        return $this->hasMany(Feed::class);
    }
}
