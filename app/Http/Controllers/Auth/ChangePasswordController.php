<?php
namespace App\Http\Controllers\Auth;

use App\User;
use Auth;
use Hash;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;

/**
 * Class ChangePasswordController
 * @package App\Http\Controllers\Auth
 */
class ChangePasswordController extends ResetPasswordController
{
    use ResetsPasswords;

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showChangePasswordForm()
    {
        return view('auth.change_password');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changePassword(Request $request)
    {
        $currentPass = Auth::user()->password;
        $this->validate($request, [
            '_token' => 'required',
            'current-password' => 'required|current_password:user,current_password,' . $currentPass,
            'password' => 'required|confirmed|min:6',
        ]);
        /** @var User $user */
        $user = User::find(Auth::user()->id);
        $user->password = Hash::make($request->get('password'));
        $user->save();
        return redirect($this->redirectPath());
    }
}
