<?php
namespace App\Services;

use App\Feed;
use App\FeedNews;
use Generator;
use SimplePie;

/**
 * Class FeedService
 * @package App\Services
 */
class FeedService
{
    const CACHE_SECONDS = 3600;
    const STORAGE_DIR = 'feeds';

    /**
     * @param Feed $feed
     * @return Generator
     */
    public function getNews(Feed $feed)
    {
        $simplePie = $this->fetch($feed->url);
        return $this->parseFeeds($simplePie, $feed->id);
    }

    /**
     * @param string $url
     * @return SimplePie
     */
    private function fetch($url)
    {
        $simplePie = new SimplePie();
        $simplePie->enable_cache(true);
        $simplePie->set_cache_location(storage_path(self::STORAGE_DIR));
        $simplePie->set_cache_duration(self::CACHE_SECONDS);
        $simplePie->set_feed_url($url);
        $simplePie->init();
        return $simplePie;
    }

    /**
     * @param SimplePie $simplePie
     * @param int $id
     * @return Generator
     */
    private function parseFeeds(SimplePie $simplePie, $id)
    {
        foreach ($simplePie->get_items() as $item) {
            yield [
                'title' => $item->get_title(),
                'link' => $item->get_link(),
                'category' => $item->get_category()->get_term(),
                'description' => strip_tags($item->get_description()),
                'feed_id' => $id,
                'uid' => md5($item->get_id()),
            ];
        }
    }

    /**
     * @param Generator $feedsData
     */
    public function saveNew(Generator $feedsData)
    {
        foreach ($feedsData as $feed) {
            if(!FeedNews::find($feed['uid'])) {
                FeedNews::create($feed);
            }
        }
    }
}
