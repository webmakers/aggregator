<?php
namespace App\Console\Commands;

use App\User;
use Closure;
use Illuminate\Console\Command;
use Illuminate\Validation\Validator;

/**
 * Class CreateUser
 * @package App\Console\Commands
 */
class CreateUser extends Command
{
    const INPUT_NAME = 'name';
    const INPUT_EMAIL = 'email';
    const INPUT_PASSWORD = 'password';

    const QUESTION_TYPE_NORMAL = 0;
    const QUESTION_TYPE_SECRET = 1;
    /** @var string */
    protected $signature = 'user:create';
    /** @var string */
    protected $description = 'Create user';

    /**
     * @return void
     */
    public function handle()
    {
        $user = new User();
        $user->name = $this->getInput(self::INPUT_NAME);
        $user->email = $this->getInput(self::INPUT_EMAIL);
        $user->password = \Hash::make($this->getInput(self::INPUT_PASSWORD));
        $user->save();
    }

    /**
     * @param string $type
     * @return string
     */
    private function getInput($type)
    {
        switch ($type) {
            case self::INPUT_NAME:
                $message = 'user name';
                $questionType = self::QUESTION_TYPE_NORMAL;
                $ruleSet = 'required|alpha|max:255';
                break;
            case self::INPUT_EMAIL:
                $message = 'email address';
                $questionType = self::QUESTION_TYPE_NORMAL;
                $ruleSet = 'required|email|max:255|unique:users';
                break;
            case self::INPUT_PASSWORD:
                $message = 'password';
                $questionType = self::QUESTION_TYPE_SECRET;
                $ruleSet = 'required|min:6';
                break;
            default :
                throw new \UnexpectedValueException();
        }
        $question = $this->askQuestion($message, $questionType);
        $validator = $this->getValidator($type, $ruleSet);
        return $this->askWithValidate($question, $validator);
    }

    /**
     * @param string $message
     * @param int $type
     * @return Closure
     */
    private function askQuestion($message, $type = self::QUESTION_TYPE_NORMAL)
    {
        return function () use ($message, $type) {
            switch ($type) {
                case  self::QUESTION_TYPE_SECRET :
                    return $this->secret($message);
                default :
                    return $this->ask($message);
            }
        };
    }

    /**
     * @param string $validatorName
     * @param string $ruleSet
     * @return Closure
     */
    private function getValidator($validatorName, $ruleSet)
    {
        return function ($value) use ($validatorName, $ruleSet) {
            return \Validator::make(
                [$validatorName => $value],
                [$validatorName => $ruleSet]
            );
        };
    }

    /**
     * @param Closure $method
     * @param Closure $callback
     * @return string
     */
    private function askWithValidate(Closure $method, Closure $callback)
    {
        $value = $method();
        /** @var Validator $validate */
        $validate = $callback($value);
        if ($validate->fails()) {
            $this->error($validate->errors()->first());
            // possible nested depth error
            $value = $this->askWithValidate($method, $callback);
        }
        return $value;
    }
}
