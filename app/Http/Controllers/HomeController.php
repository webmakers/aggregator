<?php
namespace App\Http\Controllers;

use App\Feed;
use App\FeedCategory;
use App\FeedNews;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    const ITEMS_PER_PAGE = 10;

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feeds = Feed::all();
        $categories = FeedCategory::all();
        $news = FeedNews::orderBy('created_at', 'desc')->paginate(self::ITEMS_PER_PAGE);
        return view('home', [
            'feeds' => $feeds,
            'news' => $news,
            'categories' => $categories,
            'selected_category' => null,
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function category(Request $request, $id)
    {
        $feeds = Feed::all();
        $categories = FeedCategory::all();
        $news = FeedNews::join('feeds', 'feeds.id', '=', 'feed_news.feed_id')
            ->where('feeds.feed_category_id', '=', $id)
            ->orderBy('feed_news.created_at', 'desc')
            ->select('feed_news.*')
            ->paginate(self::ITEMS_PER_PAGE);
        return view('home', [
            'feeds' => $feeds,
            'news' => $news,
            'categories' => $categories,
            'selected_category' => $id,
        ]);
    }
}
