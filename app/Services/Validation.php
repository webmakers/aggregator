<?php
namespace App\Services;

use Hash;
use Illuminate\Validation\Validator;

/**
 * Class Validation
 * @package App\Services
 */
class Validation extends Validator
{
    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return bool
     */
    public function validateCurrentPassword($attribute, $value, $parameters)
    {
        $this->requireParameterCount(1, $parameters, 'current_password');
        return Hash::check($value, $parameters[2]);
    }
}
