<?php
namespace App\Providers;

use App\Services\FeedService;
use Illuminate\Support\ServiceProvider;

/**
 * Class FeedProvider
 * @package App\Providers
 */
class FeedProvider extends ServiceProvider
{
    /** @var bool */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(FeedService::class, function () {
            return new FeedService();
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [FeedService::class];
    }
}
